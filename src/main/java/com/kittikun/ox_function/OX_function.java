package com.kittikun.ox_function;

import java.util.Scanner;

public class OX_function {

    static int count = 0;
    static char winner = '-';
    static boolean isFinish = false;
    static Scanner et = new Scanner(System.in);
    static int row, col;
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'},};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {

        while (true) {
            System.out.println("Please input Row Col: ");
            row = et.nextInt() - 1;
            col = et.nextInt() - 1;
            count++;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("You can't  not place there!!!");
            System.out.println("Please input another position.");
        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }
    static void checkX() {
        if (table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X'
                && table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X') {
            isFinish = true;
            winner = 'X';
        } else if (table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O'
                && table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O') {
            isFinish = true;
            winner = 'O';
        }
    }
    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }
    static void checkDraw() {
        if (count == 9) {
            isFinish = true;
        }
    }
    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw");
        } else {
            System.out.println("Player "+winner + " Win....");
        }
    }
    static void showBye() {
        System.out.println("Bye Bye ....");
    }
    public static void main(String[] args) {
        int count = 1;
        showWelcome();
        try {
            do {
                showTable();
                showTurn();
                input();
                checkWin();
                switchPlayer();
            } while (!isFinish);
            showResult();
            showBye();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Index out of bound");
        }

    }

}
